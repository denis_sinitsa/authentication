import os
import psycopg2
import logging

from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import Session

DB_USERNAME = os.environ['DB_USERNAME']
DB_PASSWORD = os.environ['DB_PASSWORD']
DB_URL = os.environ['DB_URL']
DB_DATABASE = os.environ['DB_DATABASE']

SQLALCHEMY_DATABASE_URL = f'postgresql://{DB_USERNAME}:{DB_PASSWORD}@{DB_URL}:5432/{DB_DATABASE}'

logger = logging.getLogger()


def _get_engine():
    try:
        engine = create_engine(
            SQLALCHEMY_DATABASE_URL
        )
    except Exception as e:
        logger.error(f'connection to db failed: {e}')
        raise Exception
    return engine


def get_reflected_tables_from_db():
    engine = _get_engine()
    metadata = MetaData()
    metadata.reflect(bind=engine)
    return metadata


def get_session():
    engine = _get_engine()
    session = Session(engine)
    return session
