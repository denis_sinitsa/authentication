import os
import json

from datetime import datetime, timedelta
from calendar import timegm
from passlib.hash import django_pbkdf2_sha256
from jose import jwt

from query import get_user,\
                  get_user_device,\
                  update_user_device,\
                  create_user_device


SECRET_KEY = os.environ['SECRET_KEY']
ALGORITHM = os.environ['ALGORITHM']
ACCESS_TOKEN_EXPIRE_MINUTES = os.environ['ACCESS_TOKEN_EXPIRE_MINUTES']
JWT_ALLOW_REFRESH = os.environ['JWT_ALLOW_REFRESH']


def verify_password(plain_password, hashed_password):
    return django_pbkdf2_sha256.verify(plain_password, hashed_password)


def create_access_token(data: dict, expires_delta):
    to_encode = data.copy()
    expire = datetime.utcnow() + expires_delta
    to_encode.update({"exp": expire})
    if JWT_ALLOW_REFRESH:
        to_encode['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def login_for_access_token(username,
                           password,
                           device_id,
                           push_token
                           ):
    user = get_user(username)
    if not user:
        return get_response(success=False, data='incorrect username')
    hashed_password = user.pop('password')
    if not verify_password(password, hashed_password):
        return get_response(success=False, data='incorrect password')
    if push_token:
        process_push_token(user['user_id'], device_id, push_token)
    if device_id:
        user.update({'device_id': device_id})
    access_token_expires = timedelta(minutes=float(ACCESS_TOKEN_EXPIRE_MINUTES))
    access_token = create_access_token(
        data=user, expires_delta=access_token_expires
    )
    return get_response(success=True, data=access_token)


def process_push_token(user_id, device_id, push_token):
    user_device_id = get_user_device(user_id, device_id)
    if user_device_id:
        update_user_device(user_device_id, push_token)
    else:
        create_user_device(user_id,
                           device_id,
                           push_token
                           )


def get_response(success: bool, data):
    return json.dumps({
        'success': success,
        'data': {
            'token': data
        },
    })
