import json
import logging

from utils import login_for_access_token, get_response

logger = logging.getLogger()
logger.setLevel(logging.ERROR)


def lambda_handler(event, context):
    if event['path'] == '/login':
        data = json.loads(event['body'])
        username = data.get('username')
        password = data.get('password')
        devise_id = data.get('device_id', None)
        push_token = data.get('push_token', None)
        response = login_for_access_token(
            username,
            password,
            devise_id,
            push_token
        )
    else:
        response = get_response(success=True, data='logged out')
    return response
