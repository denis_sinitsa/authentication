from datetime import datetime

from sqlalchemy import select, update, insert

from database import get_session, get_reflected_tables_from_db


tables_mapper = {
    'users': 'rfkeeper_server_user',
    'devices': 'rfkeeper_server_userdevice'
}


def get_table(name):
    return get_reflected_tables_from_db().tables[name]


def get_user(username):
    session = get_session()
    users_table = get_table(tables_mapper['users'])
    stmt = (
        select(users_table.c.id,
               users_table.c.username,
               users_table.c.email,
               users_table.c.password,
               ).where(users_table.c.username == username)
    )
    user = session.execute(stmt)
    user_list = user.fetchall()
    user_dict = dict(zip(['user_id', 'username', 'email', 'password'], user_list[0]))
    session.close()
    return user_dict


def get_user_device(user_id, device_id):
    session = get_session()
    device_table = get_table(tables_mapper['devices'])
    stmt = (
        select(device_table.c.id).where(device_table.c.user_id == user_id,
                                                                  device_table.c.device_id == device_id)
    )
    query = session.execute(stmt)
    user_device = query.scalar()
    session.close()
    return user_device


def update_user_device(user_device_id, push_token):
    now = datetime.now()
    session = get_session()
    session.begin()
    device_table = get_table(tables_mapper['devices'])
    stmt = (
        update(device_table).where(device_table.c.id == user_device_id).values(push_token=push_token, updated_at=now)
    )
    session.execute(stmt)
    session.commit()


def create_user_device(user_id,
                       device_id,
                       push_token
                       ):
    now = datetime.now()
    session = get_session()
    session.begin()
    device_table = get_table(tables_mapper['devices'])
    stmt = (
        insert(device_table).values(user_id=user_id,
                                    device_id=device_id,
                                    push_token=push_token,
                                    created_at=now,
                                    updated_at=now
                                    )
    )
    session.execute(stmt)
    session.commit()
